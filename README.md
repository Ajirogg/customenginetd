# CustomEngine

Custom Engine is an engine for game dev and Learning

The Starting point is in ConfigAndSetup folder.
You can use .bat file to generate project in use of your favorite IDE.
Your IDE entry point is on the root CustomEngine Folder.

Caution 
If you build for the first time with VisualStudio after a GenerateProjects, you can have a Xcopy Error. Build again and it's ok

## Setting Up
1. Clone the repository.
2. Launch the "GenerateProjects" .bat file, containt on the "ConfigAndSetup" folder, correponding to your ide.
(Example: GenerateProjectsVS2019.bat)
