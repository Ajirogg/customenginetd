echo cmd delete all files in folder
@RD "..\.vs" /s /q
@RD "..\Export" /s /q
@RD "..\GeneratedFiles" /s /q
@RD "..\Temp" /s /q
del "..\CustomEngine.sln" /s /f /q
del "..\build.gradle" /s /f /q
del "..\settings.gradle" /s /f /q
echo Done!