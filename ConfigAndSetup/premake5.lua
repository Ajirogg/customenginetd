-- Solution
workspace "CustomEngine" --Nom de la solution
   architecture "x64" --Nous n'ajouterons pas la version x86 car trop vieille
   configurations { "Debug", "Release" }
   location "../" --Position du .sln

-- Projet Engine
project "Engine" --Nom du projet (Chaque projet appartient � un workspace)
   kind "ConsoleApp"
   language "C++"
   targetdir "" --D�finit le r�pertoire pour les fichiers binaire compil�es
   location "../GeneratedFiles/Vs2019" --Position du .vcxproj

   files { "../Source/Engine/Includes/**.h", "../Source/Engine/Source/**.cpp" } --Ajout de fichiers au projet

   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"



-- Projet Game
project "Sandbox" --Nom du projet (Chaque projet appartient � un workspace)
   kind "ConsoleApp"
   language "C++"
   targetdir "" --D�finit le r�pertoire pour les fichiers binaire compil�es
   location "../GeneratedFiles/Vs2019" --Position du .vcxproj

   files { "../Source/Sandbox/Includes/**.h", "../Source/Sandbox/Source/**.cpp" } --Ajout de fichiers au projet

   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"

